package com.bai.entity;

public class Baggage {
    private double len;
    private double width;
    private double height;
    private double weight;
    private String type;

    public Baggage(double len, double width, double height, double weight, String type) {
        this.len = len;
        this.width = width;
        this.height = height;
        this.weight = weight;
        this.type = type;
    }

    public Baggage() {
    }

    public double getLen() {
        return len;
    }

    public void setLen(double len) {
        this.len = len;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
