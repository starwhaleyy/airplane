package com.bai.entity;

public class Passenger {
    private int num;
    private String coType;
    private String passType;
    private String flightType;
    private String card;
    private String ticket;
    private Baggage[] baggages;

    public Passenger(int num, String coType, String passType, String flightType, String card, String ticket, Baggage[] baggages) {
        this.num = num;
        this.coType = coType;
        this.passType = passType;
        this.flightType = flightType;
        this.card = card;
        this.ticket = ticket;
        this.baggages = baggages;
    }

    public Passenger() {
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getCoType() {
        return coType;
    }

    public void setCoType(String coType) {
        this.coType = coType;
    }

    public String getPassType() {
        return passType;
    }

    public void setPassType(String passType) {
        this.passType = passType;
    }

    public String getFlightType() {
        return flightType;
    }

    public void setFlightType(String flightType) {
        this.flightType = flightType;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public Baggage[] getBaggages() {
        return baggages;
    }

    public void setBaggages(Baggage[] baggages) {
        this.baggages = baggages;
    }
}
