package com.bai.controller;

import com.bai.entity.Baggage;
import com.bai.entity.Passenger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@CrossOrigin
public class Calculator {
    //判断普通行李是否满足国内免费行李尺寸(国内)
    public boolean isDFreeSize(Baggage baggage){
        if((baggage.getLen()<=100)&&(baggage.getWidth()<=60)&&(baggage.getHeight()<=40)){
            return true;
        }
        return false;
    }
    //悦享经济舱、超级经济舱、经济舱普通行李重量超标计算(国内)
    public double calOverMax(Baggage baggage){
        if(baggage.getWeight()>23){
            return baggage.getWeight()-23;
        }
        return 0;
    }
    //用二分查找找到小于或者等于k的最大值
    public static double uperK(List<Double> a, int k){
        if(a.size()==0){
            return 0;
        }
        Collections.sort(a);
        int l = 0;
        int r = a.size() - 1;
        //标注1： 这里是l<r，
        while(l < r){
            //标注2： 这样的操作是为了取高位
            int mid = (l + r + 1) / 2;
            if(a.get(mid) <= k) { //标注3：因为a[mid]<=k,所以a[mid]可能=k，所以mid坐标也满足条件，l = mid而不是mid+1;
                l = mid;
            }else{
                r= mid - 1; //这是a[mid] > k的时候。
            }
        }
        //标注4： 因为此时求得到的是最接近于目标值k的数，
        // 如果最小值都大于k的话，那么就没有办法得到了，所以就进行一个判断
        if(a.get(l) > k) return 0;
        //标注5： 其实这里无论返回 a[l] 还是a[r]都行，循环的退出时间是l == r 的时候
        return a.get(l);
    }
    @RequestMapping("/domestic")
    public Double domestic(@RequestBody Passenger passenger)  {
        //满足免费行李尺寸的总重量
        double sumFreeWeight=0;
        //不满足免费行李尺寸的总重量
        double sumNotFreeWeight=0;
        //单件行李超过23kg的逾重总重量和
        double overMaxWeight=0;
        //纳入免费普通行李的重量数组
        List<Double> freeWeightList=new ArrayList<>();
        //行李费用
        double expenses=0;
        //普通行李
        List<Baggage> normallist=new ArrayList<>();
        //普通行李重量
        List<Double> normal=new ArrayList<>();
        //特殊行李1重量
        List<Double> special1=new ArrayList<>();
        //特殊行李2重量
        List<Double> special2=new ArrayList<>();
        //特殊行李3重量
        List<Double> special3=new ArrayList<>();
        //特殊行李4重量
        List<Double> special4=new ArrayList<>();
        //特殊行李5重量
        List<Double> special5=new ArrayList<>();
        //特殊行李6重量
        List<Double> special6=new ArrayList<>();
        //特殊行李7重量
        List<Double> special7=new ArrayList<>();
        //特殊行李8重量
        List<Double> special8=new ArrayList<>();
        //进行行李种类分拣
        for(Baggage baggage:passenger.getBaggages()){
            if(baggage.getType().equals("normal")){
                normallist.add(baggage);
                normal.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special1")){
                special1.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special2")){
                special2.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special3")){
                special3.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special4")){
                special4.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special5")){
                special5.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special6")){
                special6.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special7")){
                special7.add(baggage.getWeight());
            }else {
                special8.add(baggage.getWeight());
            }
        }

        //计算特殊行李费用
        for(double weight:special3){
            if(weight>=2&&weight<=23){
                expenses+=2600;
            }
            else if(weight>23&&weight<=32){
                expenses+=3900;
            }
            else if(weight>32&&weight<=45){
                expenses+=5200;
            }
        }
        for(double weight:special4){
            if(weight>=2&&weight<=23){
                expenses+=1300;
            }
            else if(weight>23&&weight<=32){
                expenses+=2600;
            }
            else if(weight>32&&weight<=45){
                expenses+=3900;
            }
        }
        for(double weight:special5){
            if(weight>=2&&weight<=23){
                expenses+=490;
            }
            else if(weight>23&&weight<=32){
                expenses+=3900;
            }
        }
        for(double weight:special6){
            if(weight>=2&&weight<=23){
                expenses+=1300;
            }
            else if(weight>23&&weight<=32){
                expenses+=2600;
            }
        }
        for(double weight:special7){
            if(weight>=2&&weight<=5){
                expenses+=1300;
            }
        }
        for(double weight:special8){
            if(weight>=2&&weight<=8){
                expenses+=3900;
            }
            else if(weight>8&&weight<=23){
                expenses+=5200;
            }
            else if(weight>23&&weight<=32){
                expenses+=7800;
            }
        }
        //判断单间普通行李是否符合尺寸标准
        //判断单件普通托运行李是否重量超标
        for(Baggage baggage:normallist){
            if((baggage.getLen()+baggage.getHeight()+baggage.getWidth())<60){
                return -1.0;
            }
            if((baggage.getLen()+baggage.getHeight()+baggage.getWidth())>203){
                return -2.0;
            }
            if(baggage.getWeight()<2){
                return -3.0;
            }
            if(baggage.getWeight()>32){
                return -4.0;
            }
            //分别计算满足免费行李尺寸的总重量和不满足总重量
            if(isDFreeSize(baggage)) {
                sumFreeWeight += baggage.getWeight();
                freeWeightList.add(baggage.getWeight());
            }
            else {
                sumNotFreeWeight+=baggage.getWeight();
            }
            //单件行李超过23kg的逾重总重量和
            overMaxWeight+=calOverMax(baggage);
        }
        //将特殊行李2填入免费行李额
        for(double weight:special2){
        sumFreeWeight+=weight;
        }
        freeWeightList.addAll(special2);
        //头等舱
        if(passenger.getCoType().equals("first")){
            //成人或儿童票
            if(passenger.getPassType().equals("adultchild")){
                //vip卡情况分类
                if(passenger.getCard().equals("nothing")){
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>40){
                        expenses+=(sumFreeWeight-40)*Double.parseDouble(passenger.getTicket())*0.015;
                    }
                }
                //凤凰知音终身白金卡、白金卡
                else if(passenger.getCard().equals("card1")){
                    //额外免费一件不超过30kg的普通行李
                    sumFreeWeight=sumFreeWeight-uperK(freeWeightList,30);
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>40){
                        expenses+=(sumFreeWeight-40)*Double.parseDouble(passenger.getTicket())*0.015;
                    }


                }
                //凤凰知音金卡、银卡或星空联盟金卡
                else {
                    //额外免费一件不超过20kg的行李
                    sumFreeWeight=sumFreeWeight-uperK(freeWeightList,20);
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>40){
                        expenses+=(sumFreeWeight-40)*Double.parseDouble(passenger.getTicket())*0.015;
                    }
                }
            }
            //婴儿票
            else{
                if(passenger.getCard().equals("nothing")){
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>10){
                        expenses+=(sumFreeWeight-10)*Double.parseDouble(passenger.getTicket())*0.015;
                    }
                }
                else if(passenger.getCard().equals("card1")){
                    //额外免费一件不超过30kg的行李
                    sumFreeWeight=sumFreeWeight-uperK(freeWeightList,30);
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>10){
                        expenses+=(sumFreeWeight-10)*Double.parseDouble(passenger.getTicket())*0.015;
                    }

                }
                else {
                    //额外免费一件不超过20kg的行李
                    sumFreeWeight=sumFreeWeight-uperK(freeWeightList,20);
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>10){
                        expenses+=(sumFreeWeight-10)*Double.parseDouble(passenger.getTicket())*0.015;
                    }

                }
            }

        }
        //公务舱
        else if(passenger.getCoType().equals("business")){
            if(passenger.getPassType().equals("adultchild")){
                //vip卡情况分类
                if(passenger.getCard().equals("nothing")){
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>30){
                        expenses+=(sumFreeWeight-30)*Double.parseDouble(passenger.getTicket())*0.015;
                    }

                }
                //凤凰知音终身白金卡、白金卡
                else if(passenger.getCard().equals("card1")){
                    //额外免费一件不超过30kg的行李
                    sumFreeWeight=sumFreeWeight-uperK(freeWeightList,30);
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>30){
                        expenses+=(sumFreeWeight-30)*Double.parseDouble(passenger.getTicket())*0.015;
                    }


                }
                //凤凰知音金卡、银卡或星空联盟金卡
                else {
                    //额外免费一件不超过20kg的行李
                    sumFreeWeight=sumFreeWeight-uperK(freeWeightList,20);
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>30){
                        expenses+=(sumFreeWeight-30)*Double.parseDouble(passenger.getTicket())*0.015;
                    }
                }
            }
            else{
                if(passenger.getCard().equals("nothing")){
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>10){
                        expenses+=(sumFreeWeight-10)*Double.parseDouble(passenger.getTicket())*0.015;
                    }
                }
                else if(passenger.getCard().equals("card1")){
                    //额外免费一件不超过30kg的行李
                    sumFreeWeight=sumFreeWeight-uperK(freeWeightList,30);
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>10){
                        expenses+=(sumFreeWeight-10)*Double.parseDouble(passenger.getTicket())*0.015;
                    }

                }
                else {
                    //额外免费一件不超过20kg的行李
                    sumFreeWeight=sumFreeWeight-uperK(freeWeightList,20);
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>10){
                        expenses+=(sumFreeWeight-10)*Double.parseDouble(passenger.getTicket())*0.015;
                    }

                }
            }
        }
        //经济舱
        else{
            //单间行李超过23kg则要额外收费
            expenses+=overMaxWeight*Double.parseDouble(passenger.getTicket())*0.015;
            if(passenger.getPassType().equals("adultchild")){
                //vip卡情况分类
                if(passenger.getCard().equals("nothing")){
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>20){
                        expenses+=(sumFreeWeight-20)*Double.parseDouble(passenger.getTicket())*0.015;
                    }

                }
                //凤凰知音终身白金卡、白金卡
                else if(passenger.getCard().equals("card1")){
                    //额外免费一件不超过30kg的行李
                    sumFreeWeight=sumFreeWeight-uperK(freeWeightList,30);
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>20){
                        expenses+=(sumFreeWeight-20)*Double.parseDouble(passenger.getTicket())*0.015;
                    }


                }
                //凤凰知音金卡、银卡或星空联盟金卡
                else {
                    //额外免费一件不超过20kg的行李
                    sumFreeWeight=sumFreeWeight-uperK(freeWeightList,20);
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>20){
                        expenses+=(sumFreeWeight-20)*Double.parseDouble(passenger.getTicket())*0.015;
                    }
                }
            }
            else{
                if(passenger.getCard().equals("nothing")){
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>10){
                        expenses+=(sumFreeWeight-10)*Double.parseDouble(passenger.getTicket())*0.015;
                    }
                }
                else if(passenger.getCard().equals("card1")){
                    //额外免费一件不超过30kg的行李
                    sumFreeWeight=sumFreeWeight-uperK(freeWeightList,30);
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>10){
                        expenses+=(sumFreeWeight-10)*Double.parseDouble(passenger.getTicket())*0.015;
                    }

                }
                else {
                    //额外免费一件不超过20kg的行李
                    sumFreeWeight=sumFreeWeight-uperK(freeWeightList,20);
                    //不满足免费行李的逾重计算
                    expenses+=sumNotFreeWeight*Double.parseDouble(passenger.getTicket())*0.015;
                    //超过免费行李额
                    if(sumFreeWeight>10){
                        expenses+=(sumFreeWeight-10)*Double.parseDouble(passenger.getTicket())*0.015;
                    }

                }
            }
        }
        return expenses;
    }

    //判断是否满足国际免费行李尺寸(国际)
    public boolean isInterFreeSize(Baggage baggage){
        if((baggage.getLen()+baggage.getWidth()+baggage.getHeight())<=158){
            return true;
        }
        return false;
    }
    @RequestMapping("/international")
    public double international(@RequestBody Passenger passenger)  {
        //普通行李
        List<Baggage> normallist=new ArrayList<>();
        //普通行李重量
        List<Double> normal=new ArrayList<>();
        //特殊行李1重量
        List<Double> special1=new ArrayList<>();
        //特殊行李2重量
        List<Double> special2=new ArrayList<>();
        //特殊行李3重量
        List<Double> special3=new ArrayList<>();
        //特殊行李4重量
        List<Double> special4=new ArrayList<>();
        //特殊行李5重量
        List<Double> special5=new ArrayList<>();
        //特殊行李6重量
        List<Double> special6=new ArrayList<>();
        //特殊行李7重量
        List<Double> special7=new ArrayList<>();
        //特殊行李8重量
        List<Double> special8=new ArrayList<>();
        //进行行李种类分拣
        for(Baggage baggage:passenger.getBaggages()){
            if(baggage.getType().equals("normal")){
                normallist.add(baggage);
                normal.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special1")){
                special1.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special2")){
                special2.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special3")){
                special3.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special4")){
                special4.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special5")){
                special5.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special6")){
                special6.add(baggage.getWeight());
            }
            else if(baggage.getType().equals("special7")){
                special7.add(baggage.getWeight());
            }else {
                special8.add(baggage.getWeight());
            }
        }
        //达到免费尺寸的行李重量数组
        List<Double> freeWeightList=new ArrayList<>();
        //超尺寸的行李重量数组
        List<Double> notFreeWeightList=new ArrayList<>();
        //总费用
        double expenses=0;
        //计算特殊行李费用
        for(double weight:special3){
            if(weight>=2&&weight<=23){
                expenses+=2600;
            }
            else if(weight>23&&weight<=32){
                expenses+=3900;
            }
            else if(weight>32&&weight<=45){
                expenses+=5200;
            }
        }
        for(double weight:special4){
            if(weight>=2&&weight<=23){
                expenses+=1300;
            }
            else if(weight>23&&weight<=32){
                expenses+=2600;
            }
            else if(weight>32&&weight<=45){
                expenses+=3900;
            }
        }
        for(double weight:special5){
            if(weight>=2&&weight<=23){
                expenses+=490;
            }
            else if(weight>23&&weight<=32){
                expenses+=3900;
            }
        }
        for(double weight:special6){
            if(weight>=2&&weight<=23){
                expenses+=1300;
            }
            else if(weight>23&&weight<=32){
                expenses+=2600;
            }
        }
        for(double weight:special7){
            if(weight>=2&&weight<=5){
                expenses+=1300;
            }
        }
        for(double weight:special8){
            if(weight>=2&&weight<=8){
                expenses+=3900;
            }
            else if(weight>8&&weight<=23){
                expenses+=5200;
            }
            else if(weight>23&&weight<=32){
                expenses+=7800;
            }
        }
        for(Baggage baggage:normallist){
            if((baggage.getLen()+baggage.getHeight()+baggage.getWidth())<60){
                return -1.0;
            }
            if((baggage.getLen()+baggage.getHeight()+baggage.getWidth())>203){
                return -2.0;
            }
            if(baggage.getWeight()<2){
                return -3.0;
            }
            if(baggage.getWeight()>32){
                return -4.0;
            }
            //如果满足免费尺寸则添加重量列表
            if(isInterFreeSize(baggage)){
            freeWeightList.add(baggage.getWeight());}
            else {
                notFreeWeightList.add(baggage.getWeight());
            }
        }

        //加入特殊行李2
        freeWeightList.addAll(special2);
            //区域一(经济舱免2件不超过23kg)
            if(passenger.getFlightType().equals("region1")){
                //头等舱或公务舱
                if(passenger.getCoType().equals("first")||passenger.getCoType().equals("business")){
                    //免2件不超过32kg
                    double free1=uperK(freeWeightList,32);
                    freeWeightList.remove(free1);
                    double free2=uperK(freeWeightList,32);
                    freeWeightList.remove(free2);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=1400;
                        }
                        else if(i==1){
                            expenses+=2000;
                        }
                        else {
                            expenses+=3000;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<notFreeWeightList.size();i++){
                        expenses+=980;
                    }
                }
                //悦享经济舱或超级经济舱或经济舱
                else{
                    //免2件不超过23kg
                    double free1=uperK(freeWeightList,23);
                    freeWeightList.remove(free1);
                    double free2=uperK(freeWeightList,23);
                    freeWeightList.remove(free2);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=1400;
                        }
                        else if(i==1){
                            expenses+=2000;
                        }
                        else {
                            expenses+=3000;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<freeWeightList.size();i++){
                        if(freeWeightList.get(i)<=23){
                            expenses+=0;
                        }
                        else if(freeWeightList.get(i)<=28){
                            expenses+=380;
                        }
                        else {
                            expenses+=980;
                        }
                    }
                    for (int i=0;i<notFreeWeightList.size();i++){
                        if(notFreeWeightList.get(i)<=23){
                            expenses+=980;
                        }
                        else {
                            expenses+=1400;
                        }
                    }
                }

            }
            //区域二(经济舱免2件不超过23kg)
            else if(passenger.getFlightType().equals("region2")){
                //头等舱或公务舱
                if(passenger.getCoType().equals("first")||passenger.getCoType().equals("business")){
                    //免2件不超过32kg
                    double free1=uperK(freeWeightList,32);
                    freeWeightList.remove(free1);
                    double free2=uperK(freeWeightList,32);
                    freeWeightList.remove(free2);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=1100;
                        }
                        else if(i==1){
                            expenses+=1100;
                        }
                        else {
                            expenses+=1590;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<notFreeWeightList.size();i++){
                        expenses+=690;
                    }
                }
                //悦享经济舱或超级经济舱或经济舱
                else{
                    //免2件不超过23kg
                    double free1=uperK(freeWeightList,23);
                    freeWeightList.remove(free1);
                    double free2=uperK(freeWeightList,23);
                    freeWeightList.remove(free2);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=1100;
                        }
                        else if(i==1){
                            expenses+=1100;
                        }
                        else {
                            expenses+=1590;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<freeWeightList.size();i++){
                        if(freeWeightList.get(i)<=23){
                            expenses+=0;
                        }
                        else if(freeWeightList.get(i)<=28){
                            expenses+=280;
                        }
                        else {
                            expenses+=690;
                        }
                    }
                    for (int i=0;i<notFreeWeightList.size();i++){
                        if(notFreeWeightList.get(i)<=23){
                            expenses+=690;
                        }
                        else {
                            expenses+=1100;
                        }
                    }
                }

            }
            //区域三(经济舱免1件不超过23kg)
            else if(passenger.getFlightType().equals("region3")){
                //头等舱或公务舱
                if(passenger.getCoType().equals("first")||passenger.getCoType().equals("business")){
                    //免2件不超过32kg
                    double free1=uperK(freeWeightList,32);
                    freeWeightList.remove(free1);
                    double free2=uperK(freeWeightList,32);
                    freeWeightList.remove(free2);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=1170;
                        }
                        else if(i==1){
                            expenses+=1170;
                        }
                        else {
                            expenses+=1590;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<notFreeWeightList.size();i++){
                        expenses+=520;
                    }
                }
                //悦享经济舱或超级经济舱
                else if(passenger.getCoType().equals("economy1")){
                    //免2件不超过23kg
                    double free1=uperK(freeWeightList,23);
                    freeWeightList.remove(free1);
                    double free2=uperK(freeWeightList,23);
                    freeWeightList.remove(free2);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=1170;
                        }
                        else if(i==1){
                            expenses+=1170;
                        }
                        else {
                            expenses+=1590;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<freeWeightList.size();i++){
                        if(freeWeightList.get(i)<=23){
                            expenses+=0;
                        }
                        else if(freeWeightList.get(i)<=28){
                            expenses+=520;
                        }
                        else {
                            expenses+=520;
                        }
                    }
                    for (int i=0;i<notFreeWeightList.size();i++){
                        if(notFreeWeightList.get(i)<=23){
                            expenses+=520;
                        }
                        else {
                            expenses+=520;
                        }
                    }
                }
                //经济舱
                else {
                    //免1件不超过23kg
                    double free1=uperK(freeWeightList,23);
                    freeWeightList.remove(free1);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=1170;
                        }
                        else if(i==1){
                            expenses+=1170;
                        }
                        else {
                            expenses+=1590;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<freeWeightList.size();i++){
                        if(freeWeightList.get(i)<=23){
                            expenses+=0;
                        }
                        else if(freeWeightList.get(i)<=28){
                            expenses+=520;
                        }
                        else {
                            expenses+=520;
                        }
                    }
                    for (int i=0;i<notFreeWeightList.size();i++){
                        if(notFreeWeightList.get(i)<=23){
                            expenses+=520;
                        }
                        else {
                            expenses+=520;
                        }
                    }

                }
            }
            //区域四(经济舱免1件不超过23kg)
            else if(passenger.getFlightType().equals("region4")){
                //头等舱或公务舱
                if(passenger.getCoType().equals("first")||passenger.getCoType().equals("business")){
                    //免2件不超过32kg
                    double free1=uperK(freeWeightList,32);
                    freeWeightList.remove(free1);
                    double free2=uperK(freeWeightList,32);
                    freeWeightList.remove(free2);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=1380;
                        }
                        else if(i==1){
                            expenses+=1380;
                        }
                        else {
                            expenses+=1590;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<notFreeWeightList.size();i++){
                        expenses+=1040;
                    }
                }
                //悦享经济舱或超级经济舱
                else if(passenger.getCoType().equals("economy1")){
                    //免2件不超过23kg
                    double free1=uperK(freeWeightList,23);
                    freeWeightList.remove(free1);
                    double free2=uperK(freeWeightList,23);
                    freeWeightList.remove(free2);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=1380;
                        }
                        else if(i==1){
                            expenses+=1380;
                        }
                        else {
                            expenses+=1590;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<freeWeightList.size();i++){
                        if(freeWeightList.get(i)<=23){
                            expenses+=0;
                        }
                        else if(freeWeightList.get(i)<=28){
                            expenses+=690;
                        }
                        else {
                            expenses+=1040;
                        }
                    }
                    for (int i=0;i<notFreeWeightList.size();i++){
                        if(notFreeWeightList.get(i)<=23){
                            expenses+=1040;
                        }
                        else {
                            expenses+=2050;
                        }
                    }
                }
                //经济舱
                else {
                    //免1件不超过23kg
                    double free1=uperK(freeWeightList,23);
                    freeWeightList.remove(free1);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=1380;
                        }
                        else if(i==1){
                            expenses+=1380;
                        }
                        else {
                            expenses+=1590;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<freeWeightList.size();i++){
                        if(freeWeightList.get(i)<=23){
                            expenses+=0;
                        }
                        else if(freeWeightList.get(i)<=28){
                            expenses+=690;
                        }
                        else {
                            expenses+=1040;
                        }
                    }
                    for (int i=0;i<notFreeWeightList.size();i++){
                        if(notFreeWeightList.get(i)<=23){
                            expenses+=1040;
                        }
                        else {
                            expenses+=2050;
                        }
                    }

                }
            }
            //区域五(经济舱免1件不超过23kg)
            else {
            //头等舱或公务舱
                if(passenger.getCoType().equals("first")||passenger.getCoType().equals("business")){
                    //免2件不超过32kg
                    double free1=uperK(freeWeightList,32);
                    freeWeightList.remove(free1);
                    double free2=uperK(freeWeightList,32);
                    freeWeightList.remove(free2);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=830;
                        }
                        else if(i==1){
                            expenses+=1100;
                        }
                        else {
                            expenses+=1590;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<notFreeWeightList.size();i++){
                        expenses+=520;
                    }
                }
                //悦享经济舱或超级经济舱
                else if(passenger.getCoType().equals("economy1")){
                    //免2件不超过23kg
                    double free1=uperK(freeWeightList,23);
                    freeWeightList.remove(free1);
                    double free2=uperK(freeWeightList,23);
                    freeWeightList.remove(free2);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=830;
                        }
                        else if(i==1){
                            expenses+=1100;
                        }
                        else {
                            expenses+=1590;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<freeWeightList.size();i++){
                        if(freeWeightList.get(i)<=23){
                            expenses+=0;
                        }
                        else if(freeWeightList.get(i)<=28){
                            expenses+=210;
                        }
                        else {
                            expenses+=520;
                        }
                    }
                    for (int i=0;i<notFreeWeightList.size();i++){
                        if(notFreeWeightList.get(i)<=23){
                            expenses+=520;
                        }
                        else {
                            expenses+=830;
                        }
                    }
                }
                //经济舱
                else {
                    //免1件不超过23kg
                    double free1=uperK(freeWeightList,23);
                    freeWeightList.remove(free1);
                    //计算额外行李费用
                    for(int i=0;i<(freeWeightList.size()+notFreeWeightList.size());i++){
                        if(i==0){
                            expenses+=830;
                        }
                        else if(i==1){
                            expenses+=1100;
                        }
                        else {
                            expenses+=1590;
                        }
                    }
                    //超重或超尺寸行李收费
                    for(int i=0;i<freeWeightList.size();i++){
                        if(freeWeightList.get(i)<=23){
                            expenses+=0;
                        }
                        else if(freeWeightList.get(i)<=28){
                            expenses+=210;
                        }
                        else {
                            expenses+=520;
                        }
                    }
                    for (int i=0;i<notFreeWeightList.size();i++){
                        if(notFreeWeightList.get(i)<=23){
                            expenses+=520;
                        }
                        else {
                            expenses+=830;
                        }
                    }

                }
            }
        return expenses;
    }
}
