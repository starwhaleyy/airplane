package com.bai;

import com.bai.controller.Calculator;
import com.bai.entity.Baggage;
import com.bai.entity.Passenger;
import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.AssertionsKt;
import org.testng.annotations.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;


@SpringBootTest
public class CalculatorTests {

    private Calculator calculator=new Calculator();
    //黑盒测试

    @Test(groups = "Domestic_blackbox")
    public  void test_blackbox_Domestic1(){
        Baggage baggage1=new Baggage(20,10,60,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region0","nothing","2000",baggages);
        Assertions.assertEquals(1260.0,calculator.domestic(passenger));
        System.out.println("测试test_blackbox_Domestic1通过！");
    }
    @Test(groups = "Domestic_blackbox")
    public  void test_blackbox_Domestic2(){
        Baggage baggage1=new Baggage(20,10,60,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special3");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special4");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"business","baby","region0","card1","1000",baggages);
        Assertions.assertEquals(5575.0,calculator.domestic(passenger));
        System.out.println("测试test_blackbox_Domestic2通过！");
    }
    @Test(groups = "Domestic_blackbox")
    public  void test_blackbox_Domestic3(){
        Baggage baggage1=new Baggage(20,10,60,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special5");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special6");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy1","adultchild","region0","card2","1500",baggages);
        Assertions.assertEquals(3855.0,calculator.domestic(passenger));
        System.out.println("测试test_blackbox_Domestic3通过！");
    }
    @Test(groups = "Domestic_blackbox")
    public  void test_blackbox_Domestic4(){
        Baggage baggage1=new Baggage(20,10,60,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special7");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special8");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy2","adultchild","region0","card3","1500",baggages);
        Assertions.assertEquals(8565.0,calculator.domestic(passenger));
        System.out.println("测试test_blackbox_Domestic4通过！");
    }
    @Test(groups = "International_blackbox")
    public  void test_blackbox_International1(){
        Baggage baggage1=new Baggage(20,10,60,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region1","","",baggages);
        Assertions.assertEquals(3400.0,calculator.international(passenger));
        System.out.println("测试test_blackbox_International1通过！");
    }
    @Test(groups = "International_blackbox")
    public  void test_blackbox_International2(){
        Baggage baggage1=new Baggage(20,10,60,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region2","","",baggages);
        Assertions.assertEquals(2200.0,calculator.international(passenger));
        System.out.println("测试test_blackbox_International2通过！");
    }
    @Test(groups = "International_blackbox")
    public  void test_blackbox_International3(){
        Baggage baggage1=new Baggage(20,10,60,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region3","","",baggages);
        Assertions.assertEquals(2340.0,calculator.international(passenger));
        System.out.println("测试test_blackbox_International3通过！");
    }
    @Test(groups = "International_blackbox")
    public  void test_blackbox_International4(){
        Baggage baggage1=new Baggage(20,10,60,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region4","","",baggages);
        Assertions.assertEquals(2760.0,calculator.international(passenger));
        System.out.println("测试test_blackbox_International4通过！");
    }
    @Test(groups = "International_blackbox")
    public  void test_blackbox_International5(){
        Baggage baggage1=new Baggage(20,10,60,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region5","","",baggages);
        Assertions.assertEquals(1930.0,calculator.international(passenger));
        System.out.println("测试test_blackbox_International5通过！");
    }
    //白盒测试
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic1(){
        Baggage baggage1=new Baggage(1,1,6,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region0","nothing","2000",baggages);
        Assertions.assertEquals(-1,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic1通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic2(){
        Baggage baggage1=new Baggage(60,60,99,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region0","nothing","2000",baggages);
        Assertions.assertEquals(-2,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic2通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic3(){
        Baggage baggage1=new Baggage(20,10,60,1,"normal");
        Baggage baggage2=new Baggage(30,30,20,1.5,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region0","nothing","2000",baggages);
        Assertions.assertEquals(-3,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic3通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic4(){
        Baggage baggage1=new Baggage(20,10,60,33,"normal");
        Baggage baggage2=new Baggage(30,30,20,60,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region0","nothing","2000",baggages);
        Assertions.assertEquals(-4,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic4通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic5(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region0","card3","2600",baggages);
        Assertions.assertEquals(1365.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic5通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic6(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region0","card1","2600",baggages);
        Assertions.assertEquals(1365.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic6通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic7(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region0","nothing","2600",baggages);
        Assertions.assertEquals(1365.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic7通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic8(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","baby","region0","nothing","3000",baggages);
        Assertions.assertEquals(2295.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic8通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic9(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","baby","region0","card1","3000",baggages);
        Assertions.assertEquals(1575.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic9通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic10(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","baby","region0","card3","2000",baggages);
        Assertions.assertEquals(1530.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic10通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic11(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"business","adultchild","region0","nothing","2600",baggages);
        Assertions.assertEquals(1365.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic11通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic12(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"business","adultchild","region0","card1","2600",baggages);
        Assertions.assertEquals(1365.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic12通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic13(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"business","adultchild","region0","card3","2600",baggages);
        Assertions.assertEquals(1365.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic13通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic14(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"business","baby","region0","nothing","2600",baggages);
        Assertions.assertEquals(1989.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic14通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic15(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"business","baby","region0","card1","2600",baggages);
        Assertions.assertEquals(1365.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic15通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic16(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"business","baby","region0","card3","2600",baggages);
        Assertions.assertEquals(1989.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic16通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic17(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy2","baby","region0","nothing","2600",baggages);
        Assertions.assertEquals(2106.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic17通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic18(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy2","baby","region0","card1","2600",baggages);
        Assertions.assertEquals(1482.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic18通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic19(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy2","baby","region0","card3","2600",baggages);
        Assertions.assertEquals(2106.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic19通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic20(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy2","adultchild","region0","nothing","2600",baggages);
        Assertions.assertEquals(1716.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic20通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic21(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special1");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special1");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy2","adultchild","region0","card1","2600",baggages);
        Assertions.assertEquals(1482.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic21通过！");
    }
    @Test(groups = "Domestic_whitebox")
    public  void test_whitebox_Domestic22(){
        Baggage baggage1=new Baggage(26,55,60,20,"normal");
        Baggage baggage2=new Baggage(42,30,20,23,"special4");
        Baggage baggage3=new Baggage(30,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special6");
        Baggage baggage5=new Baggage(33,20,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy2","adultchild","region0","card3","2600",baggages);
        Assertions.assertEquals(5616.0,calculator.domestic(passenger));
        System.out.println("测试test_whitebox_Domestic22通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International1(){
        Baggage baggage1=new Baggage(1,1,6,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region0","","",baggages);
        Assertions.assertEquals(-1,calculator.international(passenger));
        System.out.println("测试test_whitebox_International1通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International2(){
        Baggage baggage1=new Baggage(60,60,99,10,"normal");
        Baggage baggage2=new Baggage(30,30,20,20,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region0","","",baggages);
        Assertions.assertEquals(-2,calculator.international(passenger));
        System.out.println("测试test_whitebox_International2通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International3(){
        Baggage baggage1=new Baggage(20,10,60,1,"normal");
        Baggage baggage2=new Baggage(30,30,20,1.5,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region0","","",baggages);
        Assertions.assertEquals(-3,calculator.international(passenger));
        System.out.println("测试test_whitebox_International3通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International4(){
        Baggage baggage1=new Baggage(20,10,60,33,"normal");
        Baggage baggage2=new Baggage(30,30,20,60,"special1");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region0","","",baggages);
        Assertions.assertEquals(-4,calculator.international(passenger));
        System.out.println("测试test_whitebox_International4通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International5(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region1","","",baggages);
        Assertions.assertEquals(4700.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International5通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International6(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy2","adultchild","region1","","",baggages);
        Assertions.assertEquals(9440.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International6通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International7(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","adultchild","region3","","",baggages);
        Assertions.assertEquals(3640.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International7通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International8(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy1","adultchild","region3","","",baggages);
        Assertions.assertEquals(6790.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International8通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International9(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy2","adultchild","region5","","",baggages);
        Assertions.assertEquals(5760.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International9通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International10(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special8");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy2","adultchild","region4","","",baggages);
        Assertions.assertEquals(11970.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International10通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International11(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special7");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy2","adultchild","region2","","",baggages);
        Assertions.assertEquals(4060.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International11通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International12(){
        Baggage baggage1=new Baggage(44,10,60,3,"normal");
        Baggage baggage2=new Baggage(50,30,20,19,"special3");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"business","adultchild","region1","","",baggages);
        Assertions.assertEquals(6000.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International12通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International13(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"business","baby","region1","","",baggages);
        Assertions.assertEquals(4700.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International13通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International14(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy1","baby","region1","","",baggages);
        Assertions.assertEquals(9440.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International14通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International15(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy1","adultchild","region1","","",baggages);
        Assertions.assertEquals(9440.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International15通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International16(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"business","baby","region4","","",baggages);
        Assertions.assertEquals(4060.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International16通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International17(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"business","baby","region5","","",baggages);
        Assertions.assertEquals(3230.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International17通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International18(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special3");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"first","baby","region5","","",baggages);
        Assertions.assertEquals(6030.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International18通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International19(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy1","baby","region4","","",baggages);
        Assertions.assertEquals(8070.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International19通过！");
    }
    @Test(groups = "International_whitebox")
    public  void test_whitebox_International20(){
        Baggage baggage1=new Baggage(20,10,60,25,"normal");
        Baggage baggage2=new Baggage(30,30,20,19,"special6");
        Baggage baggage3=new Baggage(40,50,30,26,"normal");
        Baggage baggage4=new Baggage(20,40,50,31,"special2");
        Baggage baggage5=new Baggage(10,40,66,15,"normal");
        Baggage[] baggages={baggage1,baggage2,baggage3,baggage4,baggage5};
        Passenger passenger=new Passenger(5,"economy1","baby","region5","","",baggages);
        Assertions.assertEquals(5760.0,calculator.international(passenger));
        System.out.println("测试test_whitebox_International20通过！");
    }
    @Test(groups = "uperK_whitebox")
    public void test__whitebox_uperK1(){
        List<Double> a=new ArrayList<>();
        Assertions.assertEquals(0,calculator.uperK(a,0));
        System.out.println("测试test__whitebox_uperK1通过！");
    }
    @Test(groups = "uperK_whitebox")
    public void test__whitebox_uperK2(){
        List<Double> a=new ArrayList<>();
        a.add(1.0);a.add(2.0);a.add(3.0);a.add(4.0);a.add(5.0);
        Assertions.assertEquals(0,calculator.uperK(a,0));
        System.out.println("测试test__whitebox_uperK2通过！");
    }
    @Test(groups = "uperK_whitebox")
    public void test__whitebox_uperK3(){
        List<Double> a=new ArrayList<>();
        a.add(3.0);a.add(5.0);a.add(9.0);a.add(7.0);a.add(6.0);
        Assertions.assertEquals(7,calculator.uperK(a,8));
        System.out.println("测试test__whitebox_uperK3通过！");
    }
    @Test(groups = "uperK_whitebox")
    public void test__whitebox_uperK4(){
        List<Double> a=new ArrayList<>();
        a.add(7.0);a.add(3.0);a.add(6.0);a.add(1.0);a.add(9.0);
        Assertions.assertEquals(1,calculator.uperK(a,2));
        System.out.println("测试test__whitebox_uperK4通过！");
    }
}
